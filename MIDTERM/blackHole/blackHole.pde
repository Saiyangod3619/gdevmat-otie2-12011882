
Hole target = new Hole();                                                // declaration of target and star
Hole[] star = new Hole[100];

float starNumber =100;
float gaussianX, gaussianY;
float standardDeviation =120;
float mean =0;
float posX, posY;
int j = 0;                                                               // global variable declaration. j use in checking of total star in target


void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  createRandomPosition();
  
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -( mouseY-Window.windowHeight );
  return new PVector(x,y);
}


public void createRandomPosition()                                   // function for filling of array for random position, color , scale of stars
{
  for (int i=0; i< starNumber ;i++)
    {  
      gaussianX = randomGaussian();
      gaussianY = randomGaussian();
      
      posX= standardDeviation * gaussianX + mean ;
      posY = standardDeviation * gaussianY + mean;
      
       star[i] = new Hole();
       star[i].position = new PVector (posX, posY);

       star[i].a = random(50,255);
       star[i].r = random(0,255);
       star[i].b = random(0,255);
       star[i].g = random(0,255);
       
       star[i].scale = random(5,50);
       
    }
}

void draw()
{
  background(5);
  PVector mouse = mousePos();
  
  for (int i=0; i< starNumber ;i++)                                                          // setting of random color, position and move of stars
    { star[i].setColor(star[i].r, star[i].g,star[i].b, star[i].a);
      star[i].render(); 
      star[i].moveToTarget(target.position);
    }
    
  target.position.x = mouse.x;                                                       // setting of " target / white star"
  target.position.y = mouse.y;
  target.scale = 100;
  target.render();
  target.setColor(255,255,255,255);
  
  for (int i=0; i< starNumber ;i++)                                                           // check if all star have been suck by white hole
    { 
      if (star[i].destinationTarget(target.position) == true)
      { 
        j ++;
        if (j >= starNumber)
        {
          background(5);
          createRandomPosition();
        }
      }
      else
      {
        j=0;
      }
      
    }
}
