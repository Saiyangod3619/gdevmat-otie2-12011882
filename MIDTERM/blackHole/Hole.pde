public class Hole
{
   public PVector position = new PVector();
   public float scale = 50;
   public float r = 255, g = 255, b = 255, a = 255;
   public PVector direction = new PVector();
   public float speed =3;
public void render()
   {
      fill(r,g,b,a);
      noStroke();
      circle(position.x, position.y, scale); 
   }
   
   
   public void setColor(float r, float g, float b, float a)      // function for color declaration
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
   
   public void moveToTarget(PVector targetPosition)                  // function for moving of star position to target
   {
     if (destinationTarget(targetPosition) == true)
     {
       this.position.x = targetPosition.x;
       this.position.y = targetPosition.y;
       
     }
     else
     {
       
       this.direction = PVector.sub (targetPosition,this.position);
       this.direction.normalize().mult(speed);
       this.position.add(this.direction);
      }
 }
   
   
   
   public boolean destinationTarget(PVector targetPosition)                  // function for checking of star in same position with target.  using round() function
   {
     if (round(this.position.x/10) == round(targetPosition.x/10)  && round(this.position.y/10) == round(targetPosition.y/10))
     {
       return true;
     }
     else
     {
       return false;
     }
  }
  
  
  
}
