Walker[] bigMatter = new Walker[10];
Walker[] smallMatter = new Walker[5];
void setup()
{
  size(1280, 720, P3D);
  camera(0,0,Window.eyeZ,0,0,0,0,-1,0);
  float posX = 0;
  float posY = 0;
  for (int i = 0; i<10; i++)
   { 
     posY = random(-Window.windowHeight,Window.windowHeight);
     posX =random(-Window.windowWidth,Window.windowWidth);
    bigMatter[i] = new Walker(); 
    bigMatter[i].position = new PVector(posX,posY);
    bigMatter[i].mass = 15 - i;
    bigMatter[i].scale = bigMatter[i].mass * 10;
    bigMatter[i].r = random(1,255);
    bigMatter[i].g = random(1,255);
    bigMatter[i].b = random(1,255);
    bigMatter[i].a = random(130,255);
    
   }
}

void draw()
{
  background(255);
  for(int i = 0; i < 10; i++)
  {
    for(int j = 0; j< 10; j++)
    {
      if(i != j)
      {
        bigMatter[i].update();
        bigMatter[i].render();
        bigMatter[i].applyForce(bigMatter[j].calculateAttraction(bigMatter[i]));
        bigMatter[j].applyForce(bigMatter[i].calculateAttraction(bigMatter[j]));
      }
    }    
  }
}
