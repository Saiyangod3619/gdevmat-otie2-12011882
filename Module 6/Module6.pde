public int numberWalker = 10;
Walker[] walker = new Walker[10];                                            

PVector wind = new PVector(0., -0.4);
PVector gravity = new PVector(0.15, 0.);

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  int posX = 0;
  int posY = 0;
   for (int i = 0; i<10; i++)
   {  
    
     posY =2*(Window.windowHeight/ numberWalker)*(i-(numberWalker/2));
     walker[i] = new Walker();
     walker[i].position = new PVector(-500, posY);
     walker[i].mass = numberWalker- i;
     walker[i].scale = walker[i].mass * 15;
     walker[i].r = random(1,255);
     walker[i].g = random(1,255);
     walker[i].b = random(1,255);
     walker[i].a = random(150,255);
   }
}
void draw()
{
  background(80);
  for(Walker w : walker)
  {
    float mew = 0.1f;
    if ( w.position.x >=0)
    {
       mew = 0.4f;
    }
    float normal = 1;
    float frictionMagnitude = mew * normal;
    PVector friction = w.velocity.copy();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    w.applyForce(friction);
    PVector Accelerate = new PVector(0.2,0.0);
    w.render();
    w.update();
    w.applyForce(Accelerate);
    stroke(255);
    line(0, 900, 0, -900);
    if (w.position.x >= Window.right)
    {
      w.position.x = Window.right;
      w.velocity.x *= -1;
    }
    if (w.position.y <= Window.bottom)
    {
      w.position.y = Window.bottom;
      w.velocity.y *= -1;
    }
    if (mousePressed)
    {
        background(80);
        setup();
    }
  }
}
