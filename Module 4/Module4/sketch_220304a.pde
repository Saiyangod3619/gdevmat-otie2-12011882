Walker myWalker = new Walker();
void setup ()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  myWalker.acceleration = new PVector(-0.01, 0.1);
  myWalker.scale = 30;
}
void draw()
{
  background(80);

  myWalker.update();
  myWalker.render();
  myWalker.checkEdges();
}
