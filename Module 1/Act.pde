int  wave = -100;
int speed  = 5;

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0 / 180.0),0,0,0,0,-1,0);
  
 
}
void drawCartesianPlane()
{
  background(255);
  strokeWeight(1);
  color black = color(0,0,0);
  fill(black);
  stroke(black);
  line(300,0,-300,0);
  line(0,-300,0,300); 
  
  for(int i = -300; i <= 300; i += 10)
  {
    line (i, -5, i, 5);
    line (-5, i, 5, i);
    
  }
} 
void draw()
{
  
   background(255);
   drawCartesianPlane();
   drawLinearFunction();
   drawQuadraticFunction();
   drawCircle(); 
   
     
     drawSineWave(wave);
     wave = wave + speed;
     if ( wave >= 100)
     {
       wave = -100;
     }
  
}
void drawLinearFunction()
{
  //x + 2
  //−5x + 30(purple color)
  color black = color(128,0,128);
  fill(black);
  noStroke();
   for (int x = -200; x <= 200; x++)
 {
   circle (x, (-5 * x )+ 30 , 5);
 }
}
void drawQuadraticFunction()
{
  //x^2 -15x -3
  //x2−15x−3  (yellow color)
  color black = color(255,255,0);   
  fill(black);
  stroke(black);
  for (float x = -200; x <= 200; x += 0.1f)
 {
   circle( x, ((float)Math.pow(x,2) -(x * 15) - 3), 5);
 }
}
void drawCircle()
{
  color black = color(0,0,0);
  fill(black);
  stroke(black);
  float radius = 50;
  for(int x = 0; x <= 360; x++)
  {  
    circle((float)Math.cos(x) * radius ,(float)Math.sin(x) * radius, 5);
  }
}

void drawSineWave(int wave)
{
  color green = color (0 ,255,0);
  fill (green);
  stroke (green);
  for (float x =-50; x<= 50; x += 0.1f)
  {
    circle ( wave+ x *(2*3.1416), (float)Math.sin(x)*50,5);
  }
}
