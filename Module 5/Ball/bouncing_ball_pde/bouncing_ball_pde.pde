public float numberWalker = 10;
Walker[] walker = new Walker[10];
PVector wind = new PVector(.15,0);
PVector gravity = new PVector(0,-0.4);


void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  for (int i =0; i< numberWalker; i++)
  {
    walker[i]= new Walker();
    walker[i].position = new PVector( -500,200)  ;  
   
    walker[i].mass =(i+1);
    walker[i].scale = walker[i].mass*15;
    walker[i].r = random(1,255);
    walker[i].g = random(1,255);
    walker[i].b = random(1,255);
    walker[i].a = random (150,255);
       
  } 
  
}


void draw()
{
  background(255);
  for ( Walker w : walker)
  {
   
    w.applyForce(wind);
    w.applyForce(gravity);
    w.update();
    w.setColor(w.r,w.g,w.b,w.a);
    w.render();
     
    if( w.position.y < Window.bottom)
    {
       w.velocity.y*= -1;  // newtons third law of motion
       w.position.y =Window.bottom ;
    } 
    if( w.position.x >=    Window.right )
    {
      w.velocity.x *= -1;  // newtons third law of motion
      w.position.x = Window.right;
    } 
    if (w.position.y > Window.top)
    {
      w.velocity.y *= -1;
      w.position.y = Window.top;
    }
    if( w.position.x <=    Window.left )
    {
      w.velocity.x *= -1;  // newtons third law of motion
      w.position.x = Window.left;
    }
  
  } 
}
