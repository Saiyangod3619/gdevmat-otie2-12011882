Liquid ocean = new Liquid(0,-100, Window.right, Window.bottom, 0.1f);
Walker[] myWalker = new Walker[10];
void setup()
{
  size(1280, 720, P3D);
  camera(0,0,Window.eyeZ,0,0,0,0,-1,0);
  int posX = 0;
    for (int i = 0; i<10; i++)
   {  
    posX = 2 * (Window.windowWidth /10)*(i-(10/2))+20;
    myWalker[i] = new Walker();
    myWalker[i].position = new PVector(posX, 300);
    myWalker[i].mass = 10 - i;
    myWalker[i].scale = myWalker[i].mass * 12;
    myWalker[i].r = random(1,255);
    myWalker[i].g = random(1,255);
    myWalker[i].b = random(1,255);
    myWalker[i].a = random(150,255);
   }
    /*myWalker.position = new PVector(0,400);
    myWalker.g = 0;
    myWalker.b = 0;
    myWalker.mass = 5;
    myWalker.scale = myWalker.mass * 10; 
    */
}

void draw()
{
  background(255);
  ocean.render();
  for(Walker myWalker1 : myWalker)
  {
    myWalker1.render();
    myWalker1.update();
  
  PVector gravity = new PVector(0,-0.15f * myWalker1.mass);
  myWalker1.applyForce(gravity);
  
  PVector wind = new PVector(0.1,0);
  myWalker1.applyForce(wind);
  
  float c = 0.1f;
  float normal = 1;
  float frictionMagnitude = c* normal;
  PVector friction = myWalker1.velocity.copy();
  
  myWalker1.applyForce(friction.mult(-1).normalize().mult(frictionMagnitude));
  
  if(myWalker1.position.y <= Window.bottom)
  {
    myWalker1.position.y = Window.bottom;
    myWalker1.velocity.y *= -1;
  }
  if( myWalker1.position.x >= Window.right )
  {
      myWalker1.velocity.x *= -1;  // newtons third law of motion
      myWalker1.position.x = Window.right;
  } 
  if(ocean.isCollidingWith (myWalker1))
  {
    PVector dragForce = ocean.calculateDragForce(myWalker1);
    myWalker1.applyForce(dragForce);
  }
  }
}
