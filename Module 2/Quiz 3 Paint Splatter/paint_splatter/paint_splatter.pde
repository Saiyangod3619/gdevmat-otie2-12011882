void setup()
{
  size (1020, 720 , P3D);
  camera(0,0, -(height/2.0) / tan(PI* 30.0 / 180.0 ),0, 0, 0, 0, -1 , 0);
  background(255);
}
void draw ()
{
  Gaussian myGaussian = new Gaussian();
  myGaussian.paintSplatter();
  myGaussian.render();
  
  noStroke();
  println(frameCount);
  if (frameCount >= 300)
  {
    frameCount = 0;
    
    background(255);
  }
}
