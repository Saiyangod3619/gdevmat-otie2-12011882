void setup()
{
  size(1080, 720 , P3D);
  camera(0, 0,Window.eyeZ, 0,0,0,0,-1,0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new PVector(x,y);
}
void draw()
{
  background(130);
  PVector mouse = mousePos();
  //Red
  mouse.normalize().mult(500);
  stroke(255,255,255);
  strokeWeight(10);
  line(0,0,mouse.x*0.5,mouse.y*0.5);
  line(0,0,mouse.x*-0.5,mouse.y*-0.5);
  
  mouse.normalize().mult(499);
  strokeWeight(8);
  stroke(255,0,0);
  line(0,0,mouse.x*0.5,mouse.y*0.5);
  line(0,0,mouse.x*-0.5,mouse.y*-0.5);
  
  //Black
  stroke(0,0,0);
  strokeWeight(10);
  line(0,0,mouse.x*0.2,mouse.y*0.2);
  line(0,0,mouse.x*-0.2,mouse.y*-0.2);
  
  
  //print(mouse.mag());
}
