void setup()
{
  size(1080,720,P3D);
   camera(0,0,Window.eyeZ,0,0,0,0,-1,0);
   background(255,255,255);
}
Walker myWalker = new Walker();
moveAndBounce myBoing = new moveAndBounce();
void draw()
{
  background(255,255,255);
  myWalker.render();
  myWalker.randomWalk();
  myWalker.checkEdge();
  
  myBoing.bouceUpdate();
  myBoing.render();
  
}
