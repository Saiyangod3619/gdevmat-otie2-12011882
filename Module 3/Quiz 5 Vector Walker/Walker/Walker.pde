class Walker
{
PVector posWalker = new PVector();
  void render()
  {
    circle(posWalker.x,posWalker.y,50);
    color black = color(random(255),random(255),random(255),random(50,100));
    fill(black);
    noStroke();
  }
  void randomWalk()
  {
    int rng = int(random(1,8));
    println(rng);
    if(rng==1)
    {
       posWalker.x++;
    }
    else if(rng == 2)
    {
       posWalker.x--;
    }
    else if (rng == 3)
    {
       posWalker.y++;
    }
    else if (rng == 4)
    {
       posWalker.y--;
    }
    else if (rng == 5)
    {
       posWalker.x += 5;
       posWalker.y += 5;
    }
     else if (rng == 6)
    {
       posWalker.x -= 5;
       posWalker.y -= 5;
    }   
    else if (rng == 7)
    {
       posWalker.x += 5;
       posWalker.y -= 5; 
    }
    else if (rng == 8)
    {
       posWalker.x -= 5;
       posWalker.y += 5;
    }
  }
   void checkEdge()
   {
    if (( posWalker.x > Window.right)|| ( posWalker.x < Window.left))
     {
       posWalker.x =  0;
       posWalker.y = 0 ;
     }
     if (( posWalker.y > Window.top)|| ( posWalker.y < Window.bottom))  
     {
       posWalker.y = 0 ; 
        posWalker.x =  0;
     }
   }
}
